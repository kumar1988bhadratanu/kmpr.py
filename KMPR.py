a=6
b=8
print(id(a))
print(type(a))
#TYPE CASTING
#process of converting a data from one form to another form
#INDEXING: Position of characters in a string
#SLICING: It is used for getting group of characters in a set of range
# A[Starting index:ending index]
#Extended slicing
#Similar to slicing but one step count is there by default

X="Kumar"
Y=X.replace('r','l')
print(Y)
Z="A".join(X)
print(Z)
Z1=" ".join(reversed(X))
print(Z1)
class C1:
    def m1(self,a,b):
        print(a+b)
    def m2(self,x,y):
        print(x*y)
    def main(self):
        X=C1()

        X.m1(20,30)
        X.m2(5,4)

    if "__name__"=="__main__":
            main()
# while loop
i=0
while(i<6):
    print(i)
    i+=1
#for loop
for i in range(0,5):
    print(i)
#Iteration
V=['a','e','i','o','u']
S='Bangaluru'
for i in S:
    if i in V:
        pass
    else:
        print(i)
L=[2,3,4,5,6,7,8]
EVEN=[]
ODD=[]
for i in L:
    if i%2==0:
        EVEN.append(i)
    else:
        ODD.append(i)
print(EVEN)
print(ODD)
#To add values to a dictionary
D={'a':3,'b':5,'c':8}
D['d']=10
print(D)
print(D.keys())
print(D.values())
print(D.items())
for K,V in D.items():
    print('key:',K)
    print('value',V)
a=[i for i in range(10)if i%2==0]
print(a)
a={(i,i*i) for i in range(10)}
print(a)

l1=[]
for i in range(5):
    l1.append((i,i*i))
print(l1)
#from math import pi
#r=int(input("radius of the circle"))
#r=2
#A=(pi*r**2)
#print(str(A))
#To find the maximum of three numbers
N1=20
N2=50
N3=60
if N1>N2 and N1>N3:
    max_of_three = N1
elif N2>N1 and N2>N3:
    max_of_three = N2
else:
    max_of_three = N3

print(max_of_three)
#using function

def max_of_three(N1,N2,N3):
    if N1>N2 and N1>N3:
        max_0f_three=N1
    elif N2>N1 and N2>N3:
        max_of_three=N2
    else:
        max_of_three=N3
    print(max_of_three)
max_of_three(N1,N2,N3)
# TO sum all numbers in a list
L=[20,30,40,60]
total=0
for i in L:
    total+=i
    print(total)
#using function
def total(L):
    total=0
    for i in L:
        total+=i
    print(total)
total([20,30,40,60])
#Write a python function to check whether a string is palindrome or not


#string='BHADRATANU'
#from reverse_string _import_reverse_string

#if string == reverse_string(string):

   # print("Given string is palindrome")

#else:

  #  print("string is not palindrome")
# To print unique elements from list
L1=[10,11,12,13,13,13,14,14,15]
L2=[]
for i in L1:
    if i  not in L2:
        L2.append(i)
print(L2)
#using function
def unique_elements(L2):
    for i in L1:
        if i not in L2:
            L2.append(i)
    print(L2)
unique_elements(L2)
#To count no of uppercase and lowercase in a string
S='KUMar Bhadratanu'
upper=0
lower=0
for i in S:
    if i.isupper():
        upper+=1
    elif i.islower():
            lower+=1
print(upper)
print(lower)
#Datetime
import datetime
from datetime import timedelta
x=datetime.datetime.now()
print(x)
print(type(x))
print(x.date())
print(x.month)
print(x.year)
tomorrow=x+timedelta(days=1)
next_year=x+timedelta(days=365)
next_hour=x+timedelta(hours=1)
print('tomorrow:',tomorrow)
print("next_year",next_year)
print('next_hour',next_hour)
print(timedelta.min)
print(timedelta.max)












